
import copy
import random
import numpy as np
import operator

from collections import Counter
from common import BaseSATSolver


class MOM(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        # MOM Heuristic
        # Pick literal that occurs most of the time
        k = 1

        mini = np.inf
        for clause in cnf:
            tmp_len = len(clause)
            if tmp_len < mini:
                mini = tmp_len

        MOM_c1 = Counter()
        for clause in cnf:
            if len(clause) == mini:
                for l in clause:
                    MOM_c1[l] += 1

        MOM_c2 = Counter(l for l in MOM_c1 if l > 0)
        for l in MOM_c2:
            MOM_c2[l] = 2**k * (MOM_c1[l] + MOM_c1[-l]) \
                        + (MOM_c1[l] * MOM_c1[-l])

        l = max(MOM_c2.items(), key=operator.itemgetter(1))[0]

        l = l if MOM_c1[l] >= MOM_c1[-l] else -l

        return l


class MOMmin(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        # MOM Heuristic
        # Pick literal that occurs most of the time
        k = 1

        mini = np.inf
        for clause in cnf:
            tmp_len = len(clause)
            if tmp_len < mini:
                mini = tmp_len

        MOM_c1 = Counter()
        for clause in cnf:
            if len(clause) == mini:
                for l in clause:
                    MOM_c1[l] += 1

        MOM_c2 = Counter(l for l in MOM_c1 if l > 0)
        for l in MOM_c2:
            MOM_c2[l] = 2**k * (MOM_c1[l] + MOM_c1[-l]) \
                        + (MOM_c1[l] * MOM_c1[-l])

        l = min(MOM_c2.items(), key=operator.itemgetter(1))[0]

        l = l if MOM_c1[l] >= MOM_c1[-l] else -l

        return l
