#!/bin/python3

import argparse
import time
import os
import pandas as pd
from collections import defaultdict
import numpy as np
import tqdm

from common import load_cnf_from_file
from SAT import print_sudoku
from DPLL import DPLL
from MOM import MOM, MOMmin
from JW import JW, TSJW, JWmin, TSJWmin


def main():

    parser = argparse.ArgumentParser(
        description = "Run multiple heuristics on all sudokus in a folder."
    )

    parser.add_argument("--sudoku", type=bool, nargs='?',
                        const=True, default=False,
                        help="If the problem is a sudoku, prints the sudoku"\
                        " solution on standard output.")
    parser.add_argument("-S", "--strategy_list", default=[1], action='store',
                        type=str, help='Specify the strategies using numbers')
    parser.add_argument("folder", help='Specify the foldername for the sudokus')
    parser.add_argument("nr_repeats", help='Specify the number of repeats of'\
                        'each sudoku, default is 1', default=1)

    strategies = {
        1: DPLL,
        2: MOM,
        3: JW,
        4: TSJW,
        5: MOMmin,
        6: JWmin,
        7: TSJWmin,
    }

    strategy_list = []
    args = parser.parse_args()
    for strategy in list(args.strategy_list):
        strategy_list.append(strategies[int(strategy)])
    folder = args.folder
    nr_repeats = int(args.nr_repeats)
    average = False

    data_list = []
    print("Starting timer...")
    t_start = time.time()
    pbar = tqdm.tqdm(total=len(os.listdir(folder)) * nr_repeats * len(strategy_list))
    for filename in os.listdir(folder):
        cnf = load_cnf_from_file(folder + '/' + filename)
        for strategy in strategy_list:
            solver = strategy()

            if average == True:
                data_ave = defaultdict(lambda : 0)
                for _ in range(nr_repeats):
                    solution, data = solver.solve(cnf, method)
                    for t in data:
                        if type(data[t]) == 'int' or type(data[t]) == 'float':
                            data_ave[t] += data[t] / nr_repeats
                        else:
                            data_ave[t] = data[t]
                data_ave['sudoku'] = filename
                data_list.append(data_ave)

            else:
                for _ in range(nr_repeats):
                    solution, data = solver.solve(cnf)
                    data['sudoku'] = filename
                    data_list.append(data)
                    pbar.update(1)
            if solution is None:
                print(filename + " is unsatisfiable")
            # else:
            #     print(filename + " solved " + str(nr_repeats) + " times by " \
            #           + solver.__class__.__name__)
            if args.sudoku:
                print_sudoku(solution)

    solving_time = str(time.time() - t_start)

    data_frame = pd.DataFrame(data_list)
    string_name = "S" + args.strategy_list + "_" + args.folder + "_" + \
                  args.method + "_r" + args.nr_repeats
    data_frame.to_pickle('results/' + string_name + '.infer')
    print(data_frame)
    print("Solving time: " + solving_time)

    print("Solving time: " + solving_time)


if __name__ == "__main__":
    main()
