
from common import BaseSATSolver
import random
from collections import defaultdict
import operator


class JW(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        values = defaultdict(lambda: 0)
        for clause in cnf:
            cw = 2**-len(clause)
            for l in clause:
                values[l] += cw

        l = max(values.items(), key=operator.itemgetter(1))[0]
        return l


class TSJW(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        absvalues = defaultdict(lambda: 0)
        values = defaultdict(lambda: 0)
        for clause in cnf:
            cw = 2**-len(clause)
            for l in clause:
                absvalues[abs(l)] += cw
                values[l] += cw

        l = max(absvalues.items(), key=operator.itemgetter(1))[0]
        l = l if values[l] >= values[-l] else -l
        return l


class JWmin(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        values = defaultdict(lambda: 0)
        for clause in cnf:
            cw = 2**-len(clause)
            for l in clause:
                values[abs(l)] += cw

        l = min(values.items(), key=operator.itemgetter(1))[0]
        return l


class TSJWmin(BaseSATSolver):

    def _heuristics(self, cnf, adj, ul):
        absvalues = defaultdict(lambda: 0)
        values = defaultdict(lambda: 0)
        for clause in cnf:
            cw = 2**-len(clause)
            for l in clause:
                absvalues[abs(l)] += cw
                values[l] += cw

        l = min(absvalues.items(), key=operator.itemgetter(1))[0]
        l = l if values[l] >= values[-l] else -l
        return l
