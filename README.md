

# SAT Solver

Python implementation of a SAT solver.
Accepts as input a text file with the problem in DIMACS-CNF format.

# Usage

To run the solver:

```
python3 SAT.py --sudoku -S3 <filename>
```

If the problem is a Sudoku, you can output the solution by passing `--sudoku`.

# Report

You can find a report of this project on [overleaf](https://www.overleaf.com/read/ynmgcxdffdjt).
