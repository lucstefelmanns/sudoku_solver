
from collections import defaultdict
from abc import abstractmethod, ABC
from operator import itemgetter
import copy
import time
import random
import operator


def load_cnf_from_file(filename):
    """Loads and simplify the SAT problem

    returns:
    - list of sets (clauses)
    """

    cnf = []
    with open(filename) as f:
        for line in f:
            l = line.strip()
            if l[0] == 'c' or l[0] == 'p':
                continue
            clause = {int(v) for v in l.split() if int(v) != 0}
            cnf.append(clause)
    return cnf

def print_solution(solution):
    """Prints only the true literals greater than zero"""
    print([l for l in solution if l > 0])

def is_in_cnf(cnf, l):
    """Returns true if the literal l (both l and -l) is found in the cnf"""
    for c in cnf:
        for lt in c:
            if abs(lt) == abs(l):
                return True
    return False


def save_solution_to_file(filename, solution):
    """Saves the solution set object in a file with DIMACS cnf format
    If the solution is None, the file is empty.
    """
    if solution is None:
        open(filename, 'w').close()
        return

    header = "p cnf {max_l} {ncl}\n"
    with open(filename, 'w') as f:
        f.write(header.format(max_l=max(solution), ncl=len(solution)))

        for l in solution:
            f.write(' '.join([str(l), '0\n']))


def has_tautology(clause):
    """Checks if a clause contains a tautology"""
    for c in clause:
        if c < 0:
            break
        if -c in clause:
            return True
    return False

def build_adjacency(cnf):
    """Builds the adjacency list from scratch"""
    adj = defaultdict(set)
    for idx, clause in enumerate(cnf):
        for l in clause:
            adj[l].add(idx)
    return adj


def cnf_size(cnf):
    return sum([len(c) for c in cnf])


class BaseSATSolver(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def _heuristics(self, cnf, adj, ul):
        raise NotImplementedError

    def _update_death_list(self, death_list, cnf, ul, adj, l):
        """Given a new unit literal, simplify the cnf.
        Updates ul, cnf (only the clauses), death_list and adj
        """
        assert(isinstance(l, int) and l != 0)
        ul.add(l)

        # Remove all the negations
        for clause_idx in adj[-l]:
            clause = cnf[clause_idx]
            clause.remove(-l)
            length = len(clause)
            if length == 0:
                return False
            elif length == 1 and clause[0] not in ul:
                if not self._update_death_list(death_list,
                    cnf, ul, adj, clause[0]):
                    return False

        # Remove the clauses
        for clause_idx in adj[l]:
            death_list.add(clause_idx)

        return True


    def _simplify(self, cnf):
        """Returns a tuple containing a set clauses that are not unit
        clauses.
        """
        new_cnf = []
        ul = set()
        adj = defaultdict(set)
        death_list = set()

        for clause in cnf:
            if has_tautology(clause):
                continue
            c = set()
            found_ul = False
            for l in clause:
                if -l in ul:
                    continue
                if l in ul:
                    found_ul = True
                    break;
                c.add(l)

            if found_ul:
                continue

            # Ugh, unsatisfiable
            if len(c) == 0:
                return False

            c = list(c)
            if len(c) == 1:
                if not self._update_death_list(death_list, new_cnf, ul, adj, c[0]):
                    return False
                continue

            idx = len(new_cnf)
            for l in c:
                adj[l].add(idx)
            new_cnf.append(c)

        # Remove pure literals
        self._update_death_list_pure_literals(death_list, new_cnf, adj)
        return [c for i, c in enumerate(new_cnf) if i not in death_list], ul

    def _update_death_list_pure_literals(self, death_list, cnf, adj):
        """Removes pure literals from the cnf"""
        # Remove pure literals
        for c in set(adj.keys()):
            if len(adj[-c]) == 0:
                death_list |= set(adj[c])
                death_list |= set(adj[-c])


    def _solve(self, cnf, adj, ul, l=None):

        self.data['recursion'] += 1
        self.data['new_truth_assignments'].append(len(ul))

        # Init
        if l is None:
            distr = self._heuristics(cnf, adj, ul)
            l = max(distr.items(), key=operator.itemgetter(1))[0]
            l = l if (random.uniform(0, 1) < 0.5) else -l
            s = self._solve(cnf, adj, ul, l)
            if s is not None:
                return s
            s = self._solve(cnf, adj, ul, -l)
            if s is not None:
                return s

        new_cnf = copy.deepcopy(cnf)
        new_ul = copy.deepcopy(ul)
        death_list = set()

        self._update_death_list_pure_literals(death_list, new_cnf, adj)

        # unit propagation
        if self._update_death_list(death_list, new_cnf, new_ul, adj, l) is False:
            self.data['backtrack'] += 1
            return None

        new_cnf = [c for i, c in enumerate(new_cnf) if i not in death_list]

        # Get how much we compressed the CNF
        old_size = cnf_size(cnf)
        self.data['simplification_factor'].append((old_size - cnf_size(new_cnf)) / old_size)

        if len(new_cnf) == 0:
            return new_ul

        # Rebuild the adjacency list
        new_adj = build_adjacency(new_cnf)

        distr = self._heuristics(new_cnf, new_adj, new_ul)

        assert(l_new not in ul)

        solution = self._solve(new_cnf, new_adj, new_ul, l_new)
        if solution is not None:
            return solution

        solution = self._solve(new_cnf, new_adj, new_ul, -l_new)
        if solution is not None:
            return solution

        self.data['backtrack'] += 1
        return None

    def solve(self, cnf):

        self.data = {}
        self.data['solver'] = self.__class__.__name__

        self.data['backtrack'] = 0
        self.data['solution_time'] = 0
        self.data['recursion'] = 0
        self.data['ratio'] = 0
        self.data['depth'] = 0
        self.data['ninitial_literals'] = len([c for c in cnf if len(c) == 1])
        self.data['simplification_factor'] = []
        self.data['new_truth_assignments'] = []

        if len(cnf) == 0:
            return set()

        ret = self._simplify(cnf)
        if ret is False:
            return set()
        cnf, ul = ret

        if len(cnf) == 0:
            return ul, self.data

        adj = build_adjacency(cnf)

        self.data['ratio'] = len(cnf) / len(adj)
        self.data['solution_time'] = time.time()

        solution = self._solve(cnf, adj, ul)

        self.data['solution_time'] = time.time() - self.data['solution_time']
        self.data['depth'] = self.data['recursion'] - self.data['backtrack']

        return solution, self.data
