#!/bin/python3

from collections import defaultdict
import time
import argparse
import sys
import os
from pprint import pprint
from common import save_solution_to_file, load_cnf_from_file

from DPLL import DPLL
from MOM import MOM, MOMmin
from JW import JW, TSJW, JWmin, TSJWmin


def print_sudoku(solution):
    s = [i for i in solution if i > 0]
    sudoku = []
    for n in s:
        coord = [int(d) for d in str(n)]
        x = coord[0]
        if len(sudoku) != x:
            sudoku.append([])
        sudoku[-1].append(coord[2])
    pprint(sudoku)


def main():
    parser = argparse.ArgumentParser(
        description="Implementation of a SAT solver that exploits multiple " \
                    "heuristics. The solution is saved in solution.dimacs."
    )

    parser.add_argument("--sudoku", type=bool, nargs='?',
                        const=True, default=False,
                        help="If the problem is a sudoku, prints the sudoku\
                              solution on standard output.")
    parser.add_argument("-S", '--strategy', default=1, action='store', type=int,
                        choices=range(1, 8),
                        help='Specify the strategy using an integer.')
    parser.add_argument("filename",
                        help="The file containing the problem in DIMACS format")
    args = parser.parse_args()

    strategies = {
        1: DPLL,
        2: MOM,
        3: JW,
        4: TSJW,
        5: MOMmin,
        6: JWmin,
        7: TSJWmin
        }

    solver = strategies[args.strategy]()
    filename = args.filename
    cnf = load_cnf_from_file(filename)

    t_start = time.time()
    solution, _ = solver.solve(cnf)
    print("Total Wall-time:", time.time() - t_start)

    # Save the solution even if the problem was found unsatisfiable
    output_file = os.path.splitext(filename)[0] + ".out"
    save_solution_to_file(output_file, solution)

    if solution is None:
        print("The problem is unsatisfiable.")
        sys.exit(0)

    if args.sudoku:
        print_sudoku(solution)


if __name__ == "__main__":
    main()
