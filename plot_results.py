#!/bin/python3

import pandas as pd
import argparse
import matplotlib.pyplot as plt


def main():

    parser = argparse.ArgumentParser(
        description = "Plot results of SAT solvers via stored data frame."
    )

    # parser.add_argument("name", help='Specify the name of the data frame')
    parser.add_argument("x_axis", help='Specify the x-axis to plot')
    parser.add_argument("y_axis", help='Specify the y-axis to plot')


    args = parser.parse_args()
    # name = args.name
    x_axis = args.x_axis
    y_axis = args.y_axis
    df1 = pd.read_pickle('results/S1_1000_sudokus_r10.infer')
    df2 = pd.read_pickle('results/S2_1000_sudokus_r1.infer')
    df3 = pd.read_pickle('results/S3_1000_sudokus_r1.infer')
    df4 = pd.read_pickle('results/S4_1000_sudokus_r1.infer')
    frames = [df1, df2, df3, df4]
    data_frame = pd.concat(frames)
    data_frame[data_frame['recursion'] != 0]

    # http://colorbrewer2.org/#type=qualitative&scheme=Set1&n=4
    colours = {'DPLL' : '#e41a1c', 'MOM' : '#377eb8', 'JW' : '#4daf4a',\
               'TSJW' : '#984ea3'}

    data_frame = data_frame.sort_values(by=['ratio'])

    fig, ax = plt.subplots()
    for key, grp in data_frame.groupby(['solver']):
        ax = grp.plot(ax=ax, kind='scatter', x=x_axis, y=y_axis,
                      c=colours[key], label=key, s=0.5, marker='+')
    plt.grid(color='grey', linestyle='-', linewidth=0.2)
    plt.legend(loc='best', markerscale=10)
    plt.xlabel(x_axis)
    plt.ylabel(y_axis)
    plt.yscale('symlog')
    plt.ylim(bottom=0)

    plt.savefig('results/' + x_axis + '_' + y_axis + '.png', dpi=1000,\
                bbox_inches='tight')

if __name__ == "__main__":
    main()
